//api creation
resource "azurerm_api_management_api" "example" {
  name                = var.api_name
  resource_group_name = var.resource_group_name
  api_management_name = var.apim_name
  revision            = var.revision
  display_name        = var.display_api_name
  path                = var.path
  protocols           = [var.protocols]
  

  

import {
    content_format = "swagger-json"
    content_value  = file("${path.module}/${var.Json_file}")
  }
}
//apim tag
data "azurerm_api_management" "example" {
  name                = var.apim_name
  resource_group_name = azurerm_api_management_api.example.resource_group_name
}

resource "azurerm_api_management_tag" "example" {
  api_management_id = data.azurerm_api_management.example.id
  name              = var.apim_tag
}

